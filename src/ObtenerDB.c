#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include "../include/logdb.h"

char *get_val(conexionlogdb *conexion, char *clave){
	char *buf= malloc(sizeof(char)*100);
	char *instruction = "search|";
	strcat(instruction, clave);
	send(conexion->sockfd, instruction, strlen(instruction), 0);
	recv(conexion->sockfd, buf, 100, 0);
	if (strcmp(buf, "error") == 0){
		printf("Elemento no encontrado\n");
		return NULL;
	}else{
		return buf;
	}
}
