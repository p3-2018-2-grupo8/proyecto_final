#include <sys/types.h>
#include <sys/socket.h>
#include "../include/logdb.h"

void cerrar_db(conexionlogdb *conexion){
	send(conexion->sockfd,"cerrar",7,0);
	close(conexion->sockfd);
	free(conexion);
}
