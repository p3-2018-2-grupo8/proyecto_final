#include "../include/uthash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

char* abrirdbcl(int client_fd, char* rutadb, const char* argv[1]){
	char* rutaantigua = rutdb;
	char* ruta = NULL;
	int fd = nombredb(client_fd, argv[1], &ruta);

	if (fd == 0) 
		fd = open_db(ruta);
	if (fd == 0){
		rutdb= ruta;
		printf(" Se abre la base\n");
	} else {
		printf("No se puede abrir la base\n");
	}
	/*if (fd == 0 && rutaantigua != NULL && strcmp(ruta, rutaantigua) != 0){
		fd = close_db(rutaantigua);

		if (fd== 0){
			free(rutaantigua);
		} else {
			printf("No se cierra la base"\n);
			free(ruta);
			rutadb = rutaantigua;
		}
	}*/
	return rutdb;
		
}
char* creardbcl(int client_fd, char* rutdb, const char* argv[1]){

	char* rutaantigua = rutdb;
	char* ruta = NULL;
	int fd = nombredb(client_fd, argv[1], &ruta);

	if (fd == 0) 
		fd = create_db(ruta);
	if (fd == 0){
		rutdb= ruta;
		printf(" Se crea la base\n");
	} else {
		printf("No se puede crear la base\n");
	}
	
	/*if (fd == 0 && rutaantigua != NULL && strcmp(ruta, rutaantigua) != 0){
		fd = close_db(rutaantigua);

		if (fd== 0){
			free(rutaantigua);
		} else {
			printf("No se cierra la base"\n);
			free(ruta);
			rutadb = rutaantigua;
		}
	}*/

	return rutadb;
}

void putvalcl(int client_fd, const char* rutadb){

	char* clavebuf = NULL;
	int fd= obtener(client_fd, &clavebuf);

	char* valorbuf = NULL;
	if (fd== 0)
		fd= obtener(client_fd, &valorbuf);

	if (fd == 0){
		fd = put_val(rutadb, clavebuf, valorbuf);
		printf("Se agrega valor\n");

	} else {
		printf("No se agrega valor"\n);
	}

	free(clavebuf);
	free(valorbuf);

	return;
}
void getvalcl(int client_fd, const char* rutadb){
	char* clavebuf = NULL;
	int fd;
	int resc;
	resc = fd;
	fd = obtener(client_fd, &clavebuf);
	int resv;
	char* valorbuf = NULL;
	if (fd == 0)
		resv = fd;
		fd = get_val(rutadb, clavebuf, &valorbuf);

	uint32_t valor;
	if (fd == 0){
		valor  = (uint32_t)(1 + strlen(valorbuf))
		uint32_t n = htonl(valor);
		ssize_t esc = write(client_fd, &n, sizeof(uint32_t));
		if (n < sizeof(uint32_t)){
			fd= -1;
		}
	}
	if (fd== 0){
		ssize_t esc= write(client_fd, valorbuf, valor);
		if (esc< valor){
			fd = -1;
		}
	if (resc == 0) 
	free(valorbuf);
	if (resv == 0) 
	free(clavebuf);
	return;
}
void delvalcl(int client_fd, const char* rutadb){

	char* clavebuf = NULL;
	int fd = obtener(client_fd, &clavebuf);
	
	if (fd== 0) 
		result = delete_val(rutadb, clavebuf);
	printf("Se elimino el valor");
	} else {
		printf("No se elimino valor");
	}

	free(clavebuf);
	return;
}

 void close(int client_fd, char* rutadb){
	int fd = close_db(rutadb);

	if (fd== 0){
		fprintf(" Se cerró la base ");
	} else {
		printf(" No se cerró la base );
	}
	free(rutadb);
	close(client_fd);
	printf("Cliente desconectado\n");

	return NULL;
}

int nombredb(int client_fd, const char* argv[1], char** fdbuf){
	
	uint32_t nomdb;
	ssize_t n= read(client_fd, &nomdb, sizeof(uint32_t));
	nomdb = ntohl(nomdb);

	if (n < sizeof(uint32_t)){
		return -1;
	}
	char* nombuf = (char*)malloc(sizeof(char) * nomdb);
	n = read(client_fd,nombuf, nomdb);
	if (n < nomdb){
		free(nombuf);
		return -1;
	}
	*fdbuf = (char*)malloc(sizeof(char) * (strlen(argv[1]) +nomdb + 1));
	free(nombuf);

	return 0;
}
 int obtener(int client_fd, char** fdbuf){

	uint32_t tam;
	ssize_t n = read(client_fd, &tam, sizeof(uint32_t));
	tam = ntohl(tam);

	if (n< sizeof(uint32_t)){
		return -1;
	}

	*fdbuf = (char*) malloc(sizeof(char)*tam);	
	n= read(client_fd, *fdbuf, tam);
	if (n < tam){
		free(fdbuf);
		return -1;
	}
	return 0;
}

int main(int argc, const char* argv[]){
	 	
	if (argc != 3){
		fprintf(stderr, "Uso: %s <directorio> <ip> <puerto>\n", argv[0]);
		exit(-1);
	}
	
	int puerto=atoi(argv[3]);
	printf("Inicio del servidor.\n");
	printf("EL directorio del servidor es: [%s]\n", argv[1]);
	printf("La direccion del servidor es %s \n",argv[2]);
	printf("Puerto: %d\n\n",puerto);

	struct sockaddr_in server;
	memset(&serve, 0, sizeof(server));

	server.sin_family= AF_INET;
	server.sin_port = htons(puerto);
	server.sin_addr.s_addr=inet_addr(argv[2]);
	
	int socket_fd = socket(server.sin_family, SOCK_STREAM, 0);
	if (socket_fd < 0){
		 perror("Error de apertura de socket\n");
		 exit(-1);
	}
	
	if (bind(socket_fd, (struct sockaddr*)&server, sizeof(server))<0){
		printf( "Error enlazando puerto\n");
		close(socket_fd);
		exit(-1);
	}
	
	if (listen(socket_fd, 2048)){
		printf("Error en el listen");
		close(socket_fd);
		exit(-1);
	}
	
	
	while (1){
		printf("Esperando conexion del cliente\n");
		int client_fd = accept(socket_fd, NULL, NULL);
		printf("Se inició la conexión con el cliente\n")
		
		char* rutdb = NULL;
		while (1){
			
			uint32_t tipo;
			ssize_t n= read(client_fd, &tipo, sizeof(uint32_t));
			tipo = ntohl(tipo);
	
			if (n == 0) {
				close(client_fd);
				break;
			}

			char buf[1024];
			memset(buf, 0, 1024-1);
			n = read(client_fd,buf, tipo);
		
			if (strcmp(buf, "open_db") == 0){
				rutdb= _cli_open_database(client_fd, rutdb, argv[1]);
			} else if (strcmp(buf, "create_db") == 0){
				rutdb = creardbcl(client_fd, rutdb, argv[1]);
			} else if(strcmp(buf, "put_val") == 0){
				putvalcl(client_fd, rutdb);
			} else if (strcmp(buf, "get_val") == 0){
				getvalcl(client_fd, rutdb);
			} else if(strcmp(buf, "delete") == 0){
				delvalcl(client_fd, rutdb);
			} else if(strcmp(buf, "close_db") == 0){
				close(client_fd, rutdb);
				break;
			}
		}
	}
	close(socket_fd);
	return 0;
}





	


