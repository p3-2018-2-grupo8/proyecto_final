#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include "../include/logdb.h"

int crear_db(conexionlogdb *conexion, char *nombre_db){
	char buf[20]={0};
	char *instruction = "create|";
	strcat(instruction,nombre_db);
	send(conexion->sockfd, instruction, strlen(instruction), 0);
	recv(conexion->sockfd, buf, 20, 0);
	if (strcmp(buf, "exito") != 0){
		printf("%s", buf);
		return -1;
	}else{
		conexion->nombredb = nombre_db;
		return 0;
	}
}
