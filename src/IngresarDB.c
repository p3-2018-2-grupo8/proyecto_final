#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include "../include/logdb.h"

int put_val(conexionlogdb *conexion, char *clave, char *valor){
	char buf[20]={0};
	char *instruction = "add|";
	char *element;
	strcpy(element,clave);
	strcat(element,"|");
	strcat(element,valor);
	strcat(instruction,element);
	send(conexion->sockfd, instruction, strlen(instruction), 0);
	recv(conexion->sockfd, buf, 20, 0);
	if (strcmp(buf, "exito") != 0){
		printf("%s", buf);
		return -1;
	}else{
		return 0;
	}	
}
