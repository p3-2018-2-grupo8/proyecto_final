#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../include/logdb.h"

#define MAXSLEEP 64

int connect_retry( int domain, int type, int protocol, 	const struct sockaddr *addr, socklen_t alen){
	int numsec, fd; 
	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) { 
		if (( fd = socket( domain, type, protocol)) < 0) 
			return(-1); 
		if (connect( fd, addr, alen) == 0) { 
			return(fd); 
		} 
		close(fd); 				
		if (numsec <= MAXSLEEP/2)
			sleep( numsec); 
	} 
	return(-1); 
}

conexionlogdb *conectar_db(char *ip, int puerto){
	struct sockaddr_in direccion_cliente;
	memset(&direccion_cliente, 0, sizeof(direccion_cliente));
	direccion_cliente.sin_family = AF_INET;
	direccion_cliente.sin_port = htons(puerto);
	direccion_cliente.sin_addr.s_addr = inet_addr(ip) ;
	int fd = connect_retry(direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente));
	if (fd < 0) {
		printf("fallo de conexion");
		return NULL;
	}
	conexionlogdb *conexion =(conexionlogdb *)malloc(sizeof(conexionlogdb));
	conexion->ip = ip;
	conexion->puerto = (unsigned short)puerto;
	conexion->id_sesion = rand() % 21;
	conexion->nombredb = "";
	conexion->sockfd = fd;
	return conexion;
}
